$(document).ready(function(){
	/*   this is used for changing background color to #bdb8ac and text color is white */
	/*   * is used for all element of a web page. */
	$("*").css({
		"background-color":"#bdb8ac",
		 "color":"white"
	});

	/*  color changing when even and odd occuring */
	$('ul').hover(
			function(){
				$('li:even').css({
					"background-color":"white",
					"color":"black",
					"width":"150px"
				});
			},
			function(){
				$('li:odd').css({
					"background-color":"maroon",
					"color":"yellow",
					"width":"150px"
				});
			}
			
	);
	
	$('div:first').css({
		"width":"300px",
		"margin":"0px auto",
		"float":"left"
	});
	
	
	$('div:last').css({
		
		"width":"300px",
		"margin":"0px auto",
	});
	
	/* changing li color of fist <ul> element only*/
	$('#ul-1st-id li:nth-child(3)').css({
		"background-color":"olive",
		"color":"pink"
	});
	
});