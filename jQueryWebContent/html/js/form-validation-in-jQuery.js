$(document).ready(function(){
	$('#submit').click(function(){
		var user=$('#username').val();
		if(user==""){
			$('#user_error').html('** Username Must be Filled.');
			$('#user_error').css('color','red');
			return false;
		}
		
		if(user.length<=2 || user.length>=26){
			$('#user_error').html('** Username Must be between 3 and 25.');
			$('#user_error').css('color','red');
			return false;
		}
		
		//	validation for password
		var pass=$('#password').val();
		if(pass==""){
			$('#password_error').html('** Password Must be Filled.');
			$('#password_error').css('color','red');
			return false;
		}
		
	});
	
	$('#cpassword').keyup(function(){
		var pwd=$('#password').val();
		var cpwd=$('#cpassword').val();
		
		if(cpwd!=pwd){
			$('#cpassword_error').html('Password are not matching.');
			$('#cpassword_error').css('color','red');
			return false;
		}
		else{
			$('#cpassword_error').html('');
			return true;
		}
	});
	
});