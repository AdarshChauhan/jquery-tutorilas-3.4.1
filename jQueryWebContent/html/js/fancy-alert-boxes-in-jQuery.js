$(document).ready(function(){
		
	$('#success').click(function(){
		swal({
			  title: "Good job!",
			  text: "You clicked on right button!",
			  icon: "success",  /* u can use success,warning,error,info*/
			  button: "Ok"
			});		
	});
	
	$('#warning').click(function(){
		swal({
			  title: "Good job!",
			  text: "You clicked on near by right button!",
			  icon: "warning",  /* u can use success,warning,error,info*/
			  button: "ok! Try Again"
			});		
	});
	
	$('#error').click(function(){
		swal({
			  title: "Good job!",
			  text: "You clicked on wrong button!",
			  icon: "error",  /* u can use success,warning,error,info*/
			  button: "Try Again"
			});		
	});
	
	$('#info').click(function(){
		swal({
			  title: "Good job!",
			  text: "You can Search for more Information!",
			  icon: "info",  /* u can use success,warning,error,info*/
			  button: "Search More"
			});		
	});
	
	
});