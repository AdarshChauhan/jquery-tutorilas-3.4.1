$(function() {

	$('#btn').click(function() {
		$('#p1').css({
			"background-color" : "green",
			"color" : "white"
		});
		$('#p2').css({
			"background-color" : "black",
			"color" : "white"
		});
		$('#p3').css({
			"background-color" : "olive",
			"color" : "white"
		});
		$('#btn').css({
			"background-color" : "red",
			"color" : "white",
			"border-style" : "none",
			"margin-left" : "500px"
		});
		$('p:second').hide();
	});

	/* hover function */
	$('p').hover(function() {
		$(this).css({
			"background-color" : "red",
			"color" : "white",
			"border-style" : "none"
		});
	}, function() {
		$(this).css({
			"background-color" : "yellow",
			"color" : "red",
			"border-style" : "none"
		});
	});
		
});
