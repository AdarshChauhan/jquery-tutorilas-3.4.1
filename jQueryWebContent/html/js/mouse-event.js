$(function(){
	$('h1').mouseenter(function(){
		$(this).css({
			"background-color":"black",
			"color":"white"
		});
		
	});
	
	$('h1').mouseleave(function(){
		$(this).css({
			"background-color":"olive",
			"color":"red"
		});
		
	});
	
	/* whenever we try to select something from anywhere in the TEXT then it will work. */
	$('h1').mousedown(function(){
		$(this).css({
			"background-color":"cyan",
			"color":"black"
		});
		
	});
	
	/* whenever we try to click anywhere in the TEXT then it will work. */
	$('h1').mouseup(function(){
		$(this).css({
			"background-color":"yellow",
			"color":"black"
		});
		
	});
	
});