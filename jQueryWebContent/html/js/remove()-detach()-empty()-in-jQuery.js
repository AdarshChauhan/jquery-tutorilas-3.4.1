$(document).ready(function(){
	/* this is about remove() method.	*/			
	$('#remove').click(function(){
		$('p,span,div').remove();			
	});
	
	/* this is about empty() method.	*/
	$('#empty').click(function(){
		$('p,span,div').empty();			
	});
	
	/* this is about detach() method.	*/
	$('#detach').click(function(){
	
			$('p,span,div').detach();
				
	});
	
	/* detach and after append  or remove and after append */
	$("#detachAndAppend").click(function(){  
        $("body").append($("#p1").detach());  
    });  
    $("#removeAndAppend").click(function(){  
        $("body").append($("#p2").remove());  
    });  
    $("p").click(function(){  
        $(this).animate({fontSize: "+=1px"})  
    }); 
					
});