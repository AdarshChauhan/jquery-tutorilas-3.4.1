$(document).ready(function(){
	$('input').keypress(function(){
		$(this).css({
			"background-color":"black",
			"color":"white",
			"width":"300px",
			"padding":"25px",
			"height":"25px",
			
			"border-radius":"15px"
		});
	});
	
	$('input').keydown(function(){
		$(this).css({
			"background-color":"red",
			"color":"white"
		});
	});
	
	$('input').keyup(function(){
		$(this).css({
			"background-color":"cyan",
			"color":"red",
			"width":"auto",
			"padding":"1px",
			"border-radius":"0px"
		});
	});
	
});